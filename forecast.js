function countSMA(){
    let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
      var data= JSON.parse(sessionStorage.getItem('datapem'));
      var jeda =+sessionStorage.getItem('bulansma');
      var ramal=[];

    for(var x=0;x< bulan.length;x++){
      var peramalan;
      if(x<jeda){
        peramalan='';
        ramal[x]=0;
      }
      else{
        peramalan=0;
        for(var y=x-jeda;y< bulan.length;y++){
          if(y<x)peramalan+=parseInt(data[y]);
          else if(y>x) break;
        }
       peramalan=peramalan/jeda;
       ramal[x]=Math.round(peramalan);
      }
    }
    
    var xi=0;
    var fi=0;
    var kesalahan=0;
    var kesalahanA=0;
    var kesalahanK=0;
    var kesalahanP=0;
    var kesalahanPA=0;
    for(var x=0;x< bulan.length;x++){
      if(x<jeda){
        continue;
      }
      else{
        xi+=parseInt(data[x]);
        fi+=ramal[x];
        kesalahan+=(parseInt(data[x])-ramal[x]);
        kesalahanA+=Math.abs((parseInt(data[x])-ramal[x]));
        kesalahanK+=Math.pow((parseInt(data[x])-ramal[x]),2);
        kesalahanP+=((parseInt(data[x])-ramal[x])/parseInt(data[x])*100);
        kesalahanPA+=(Math.abs((parseInt(data[x])-ramal[x]))/parseInt(data[x])*100);
        
      }
    }

    var ME=Math.round((Math.abs(kesalahan)/(12-jeda))*100)/100;
    var MAE=Math.round((kesalahanA/(12-jeda))*100)/100;
    var MSE=Math.round((kesalahanK/(12-jeda))*100)/100;
    var MAPE=Math.round((kesalahanPA/(12-jeda))*100)/100;

    sessionStorage.setItem('meSMA',ME);
    sessionStorage.setItem('maeSMA',MAE);
    sessionStorage.setItem('mseSMA',MSE);
    sessionStorage.setItem('mapeSMA',MAPE);
    return ramal;
  }

  function countDMA(){
    let $ = require('jquery');
    const remote = require('electron').remote;
    const app = remote.app;
    var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
    var data= JSON.parse(sessionStorage.getItem('datapem'));
    var jeda =2;
    var ramal=[];
    var st=[];
    var sst=[];
    var at=[];
    var bt=[];

  for(var x=0;x< bulan.length;x++){
    if(x==0){
      st[x]=0;
      sst[x]=0;
      at[x]=0;
      bt[x]=0;
      ramal[x]=0;
    }
    else{
      if(x<jeda){
      st[x]=Math.round((parseInt(data[x])+parseInt(data[x-1]))/2);
      sst[x]=0;
      at[x]=0;
      bt[x]=0;
      ramal[x]=0;
      }
      else{
      st[x]=Math.round((parseInt(data[x])+parseInt(data[x-1]))/2);
      sst[x]=Math.round((st[x]+st[x-1])/2);
      at[x]=(2*st[x])-sst[x];
      bt[x]=(2/(2-1))*(st[x]-sst[x]);
      if(x==jeda) ramal[x]=0;
      else ramal[x]=at[x-1]+bt[x-1];
      }
     
    }
  }
  
  var xi=0;
  var fi=0;
  var kesalahan=0;
  var kesalahanA=0;
  var kesalahanK=0;
  var kesalahanP=0;
  var kesalahanPA=0;
  for(var x=0;x< bulan.length;x++){
    if(x<=jeda){
      continue;
    }
    else{
      xi+=parseInt(data[x]);
      fi+=ramal[x];
      kesalahan+=(parseInt(data[x])-ramal[x]);
      kesalahanA+=Math.abs((parseInt(data[x])-ramal[x]));
      kesalahanK+=Math.pow((parseInt(data[x])-ramal[x]),2);
      kesalahanP+=((parseInt(data[x])-ramal[x])/parseInt(data[x])*100);
      kesalahanPA+=(Math.abs((parseInt(data[x])-ramal[x]))/parseInt(data[x])*100);
      
    }
  }
  
  var ME=Math.round((Math.abs(kesalahan)/(12-(jeda+1)))*100)/100;
    var MAE=Math.round((kesalahanA/(12-(jeda+1)))*100)/100;
    var MSE=Math.round((kesalahanK/(12-(jeda+1)))*100)/100;
    var MAPE=Math.round((kesalahanPA/(12-(jeda+1)))*100)/100;

    sessionStorage.setItem('meDMA',ME);
    sessionStorage.setItem('maeDMA',MAE);
    sessionStorage.setItem('mseDMA',MSE);
    sessionStorage.setItem('mapeDMA',MAPE);
    return ramal;
  }

  function countSES(){
    let $ = require('jquery');
    const remote = require('electron').remote;
    const app = remote.app;
    var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
    var data= JSON.parse(sessionStorage.getItem('datapem'));
    var ses =+sessionStorage.getItem('alfases');
    var jeda=1;
    var ramal=[];

  for(var x=0;x< bulan.length;x++){
    var peramalan;
    if(x<jeda){
      peramalan='';
      ramal[x]=0;
    }
    else if(x==jeda){
      peramalan=parseInt(data[x-1]);
      ramal[x]=parseInt(data[x-1]);
    }
    else{
      peramalan=0;
      peramalan=(ses*parseInt(data[x-1]))+((1-ses)*ramal[x-1]);
      ramal[x]=Math.round(peramalan);
    }

  }
  
  var xi=0;
  var fi=0;
  var kesalahan=0;
  var kesalahanA=0;
  var kesalahanK=0;
  var kesalahanP=0;
  var kesalahanPA=0;
  for(var x=0;x< bulan.length;x++){
    if(x<jeda){
      continue;
    }
    else{
      xi+=parseInt(data[x]);
      fi+=ramal[x];
      kesalahan+=(parseInt(data[x])-ramal[x]);
      kesalahanA+=Math.abs((parseInt(data[x])-ramal[x]));
      kesalahanK+=Math.pow((parseInt(data[x])-ramal[x]),2);
      kesalahanP+=((parseInt(data[x])-ramal[x])/parseInt(data[x])*100);
      kesalahanPA+=(Math.abs((parseInt(data[x])-ramal[x]))/parseInt(data[x])*100);
      
    }
  }
  
  var ME=Math.round((Math.abs(kesalahan)/(12-jeda))*100)/100;
    var MAE=Math.round((kesalahanA/(12-jeda))*100)/100;
    var MSE=Math.round((kesalahanK/(12-jeda))*100)/100;
    var MAPE=Math.round((kesalahanPA/(12-jeda))*100)/100;

    sessionStorage.setItem('meSES',ME);
    sessionStorage.setItem('maeSES',MAE);
    sessionStorage.setItem('mseSES',MSE);
    sessionStorage.setItem('mapeSES',MAPE);
    return ramal;
}

function countDES(){
    let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
      var data= JSON.parse(sessionStorage.getItem('datapem'));
      var des=+sessionStorage.getItem('alfades');
      var jeda =2;
      var ramal=[];
      var st=[];
      var sst=[];
      var at=[];
      var bt=[];

    for(var x=0;x< bulan.length;x++){
      var peramalan;
      if(x==0){
        peramalan='';
        st[x]=parseInt(data[x]);
        sst[x]=parseInt(data[x]);
        at[x]=0;
        bt[x]=0;
        ramal[x]=0;
      }
      else{
        st[x]=Math.round((des*parseInt(data[x]))+((1-des)*st[x-1]));
        sst[x]=Math.round((des*st[x])+((1-des)*sst[x-1]));
        at[x]=(2*st[x])-sst[x];
        bt[x]=Math.round((des/(1-des))*(st[x]-sst[x]));
      
       if(x<jeda)
         ramal[x]=0;
       else
         ramal[x]=at[x-1]+bt[x-1];
      }
    }
    
    var xi=0;
    var fi=0;
    var kesalahan=0;
    var kesalahanA=0;
    var kesalahanK=0;
    var kesalahanP=0;
    var kesalahanPA=0;
    for(var x=0;x< bulan.length;x++){
      if(x<jeda){
        continue;
      }
      else{
        xi+=parseInt(data[x]);
        fi+=ramal[x];
        kesalahan+=(parseInt(data[x])-ramal[x]);
        kesalahanA+=Math.abs((parseInt(data[x])-ramal[x]));
        kesalahanK+=Math.pow((parseInt(data[x])-ramal[x]),2);
        kesalahanP+=((parseInt(data[x])-ramal[x])/parseInt(data[x])*100);
        kesalahanPA+=(Math.abs((parseInt(data[x])-ramal[x]))/parseInt(data[x])*100);
        
      }
    }

    var ME=Math.round((Math.abs(kesalahan)/(12-jeda))*100)/100;
    var MAE=Math.round((kesalahanA/(12-jeda))*100)/100;
    var MSE=Math.round((kesalahanK/(12-jeda))*100)/100;
    var MAPE=Math.round((kesalahanPA/(12-jeda))*100)/100;

    sessionStorage.setItem('meDES',ME);
    sessionStorage.setItem('maeDES',MAE);
    sessionStorage.setItem('mseDES',MSE);
    sessionStorage.setItem('mapeDES',MAPE);
    return ramal;
  }

  function countWMA(){
    let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
      var data= JSON.parse(sessionStorage.getItem('datapem'));
      var jeda =3;
      var ramal=[];

    for(var x=0;x< bulan.length;x++){
      var peramalan;
      if(x<jeda){
        peramalan='';
        ramal[x]=0;
      }
      else{
        peramalan=0;
        var g=1;
        for(var y=x-jeda;y< bulan.length;y++){
          if(y<x){
            peramalan+=(parseInt(data[y])*g);
            g++;
          }
          else if(y>x) break;
        }
       peramalan=peramalan/6;
       ramal[x]=Math.round(peramalan);
      }
      
    }
    
    var xi=0;
    var fi=0;
    var kesalahan=0;
    var kesalahanA=0;
    var kesalahanK=0;
    var kesalahanP=0;
    var kesalahanPA=0;
    for(var x=0;x< bulan.length;x++){
      if(x<jeda){
        continue;
      }
      else{
        xi+=parseInt(data[x]);
        fi+=ramal[x];
        kesalahan+=(parseInt(data[x])-ramal[x]);
        kesalahanA+=Math.abs((parseInt(data[x])-ramal[x]));
        kesalahanK+=Math.pow((parseInt(data[x])-ramal[x]),2);
        kesalahanP+=((parseInt(data[x])-ramal[x])/parseInt(data[x])*100);
        kesalahanPA+=(Math.abs((parseInt(data[x])-ramal[x]))/parseInt(data[x])*100);
        
      }
    }
    var ME=(Math.abs(kesalahan)/(12-jeda))*100/100;
    var MAE=Math.round((kesalahanA/(12-jeda))*100)/100;
    var MSE=Math.round((kesalahanK/(12-jeda))*100)/100;
    var MAPE=Math.round((kesalahanPA/(12-jeda))*100)/100;

    sessionStorage.setItem('meWMA',ME);
    sessionStorage.setItem('maeWMA',MAE);
    sessionStorage.setItem('mseWMA',MSE);
    sessionStorage.setItem('mapeWMA',MAPE);
    return ramal;
  }

  function countLinier(){
    let $ = require('jquery');
    const remote = require('electron').remote;
    const app = remote.app;
    var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
    var data= JSON.parse(sessionStorage.getItem('datapem'));
    var jeda =0;
    var ramal=[];
   
    var t=0;
    var yt=0;
    var tyt=0;
    var t2=0;

    for(var x=0;x< bulan.length;x++){
      t+=(x+1);
      yt+=parseInt(data[x]);
      tyt+=(parseInt(data[x])*(x+1));
      t2+=(Math.pow((x+1),2));
    }

    
    var b=Math.round(((12*tyt)-(yt*t))/((12*t2)-Math.pow(t,2)));
    var a=(yt-(b*t))/12;
    
    for(var x=0;x< bulan.length;x++){
      var peramalan=Math.round(a+(b*(x+1)));
      ramal[x]=peramalan;

    }
    
    var xi=0;
    var fi=0;
    var kesalahan=0;
    var kesalahanA=0;
    var kesalahanK=0;
    var kesalahanP=0;
    var kesalahanPA=0;

    for(var x=0;x< bulan.length;x++){
     
        xi+=parseInt(data[x]);
        fi+=ramal[x];
        kesalahan+=(parseInt(data[x])-ramal[x]);
        kesalahanA+=Math.abs((parseInt(data[x])-ramal[x]));
        kesalahanK+=Math.pow((parseInt(data[x])-ramal[x]),2);
        kesalahanP+=((parseInt(data[x])-ramal[x])/parseInt(data[x])*100);
        kesalahanPA+=(Math.abs((parseInt(data[x])-ramal[x]))/parseInt(data[x])*100);
        
    }
    
    var ME=Math.round((Math.abs(kesalahan)/(12-jeda))*100)/100;
    var MAE=Math.round((kesalahanA/(12-jeda))*100)/100;
    var MSE=Math.round((kesalahanK/(12-jeda))*100)/100;
    var MAPE=Math.round((kesalahanPA/(12-jeda))*100)/100;
    
    sessionStorage.setItem('meLIN',ME);
    sessionStorage.setItem('maeLIN',MAE);
    sessionStorage.setItem('mseLIN',MSE);
    sessionStorage.setItem('mapeLIN',MAPE);
    return ramal;
  }