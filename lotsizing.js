function countDMA(){
    let $ = require('jquery');
    const remote = require('electron').remote;
    const app = remote.app;
    var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
    var data= JSON.parse(sessionStorage.getItem('datapem'));
    var bahan=+JSON.parse(sessionStorage.getItem('bahan'));
    var jeda =2;
    var ramal=[];
    var st=[];
    var sst=[];
    var at=[];
    var bt=[];
    var m=2;

  for(var x=0;x< bulan.length;x++){
    if(x==0){
      st[x]=0;
      sst[x]=0;
      at[x]=0;
      bt[x]=0;
      ramal[x]=0;
    }
    else{
      if(x<jeda){
      st[x]=Math.round((parseInt(data[x])+parseInt(data[x-1]))/2);
      sst[x]=0;
      at[x]=0;
      bt[x]=0;
      ramal[x]=0;
      }
      else{
      st[x]=Math.round((parseInt(data[x])+parseInt(data[x-1]))/2);
      sst[x]=Math.round((st[x]+st[x-1])/2);
      at[x]=(2*st[x])-sst[x];
      bt[x]=(2/(2-1))*(st[x]-sst[x]);
      if(x==jeda) ramal[x]=0;
      else ramal[x]=(at[x-1]+(bt[x-1]*m))*bahan;
      }
     
    }
  }
  
    return ramal;
  }


function countDES(){
    let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      var bahan=+JSON.parse(sessionStorage.getItem('bahan'));
      var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
      var data= JSON.parse(sessionStorage.getItem('datapem'));
      var des=+sessionStorage.getItem('alfades');
      var jeda =2;
      var ramal=[];
      var st=[];
      var sst=[];
      var at=[];
      var bt=[];
      var m=2;

    for(var x=0;x< bulan.length;x++){
      var peramalan;
      if(x==0){
        peramalan='';
        st[x]=parseInt(data[x]);
        sst[x]=parseInt(data[x]);
        at[x]=0;
        bt[x]=0;
        ramal[x]=0;
      }
      else{
        st[x]=Math.round((des*parseInt(data[x]))+((1-des)*st[x-1]));
        sst[x]=Math.round((des*st[x])+((1-des)*sst[x-1]));
        at[x]=(2*st[x])-sst[x];
        bt[x]=Math.round((des/(1-des))*(st[x]-sst[x]));
      
       if(x<jeda)
         ramal[x]=0;
       else
         ramal[x]=(at[x-1]+(bt[x-1]*m))*bahan;
      }
    }
    
    return ramal;
  }

function countLinier(){
    let $ = require('jquery');
    const remote = require('electron').remote;
    const app = remote.app;
    var bulan= JSON.parse(sessionStorage.getItem('namabulan'));
    var data= JSON.parse(sessionStorage.getItem('datapem'));
    var bahan=+JSON.parse(sessionStorage.getItem('bahan'));
    //app.console.log(bahan);
    var jeda =0;
    var ramal=[];
   
    var t=0;
    var yt=0;
    var tyt=0;
    var t2=0;

    for(var x=0;x< bulan.length;x++){
      t+=(x+1);
      yt+=parseInt(data[x]);
      tyt+=(parseInt(data[x])*(x+1));
      t2+=(Math.pow((x+1),2));
    }

    
    var b=Math.round(((12*tyt)-(yt*t))/((12*t2)-Math.pow(t,2)));
    var a=(yt-(b*t))/12;
    
    var y=13;
    for(var x=0;x<bulan.length;x++){
      var peramalan=Math.round(a+(b*(y)));
      ramal[x]=Math.ceil(peramalan*bahan);
      y++;
    }
    
    return ramal;
  }

  function getPorec(a,b,c,ctr){
    let $ = require('jquery');
      const remote = require('electron').remote;
      const app = remote.app;
      
      var bulan=JSON.parse(sessionStorage.getItem('namabulan'));
      var hari=JSON.parse(sessionStorage.getItem('hari'));
      var method=+sessionStorage.getItem('method');
      if(method==5) var ramal=countLinier();
      else if(method==4)var ramal=countDES();
      else if(method==1)var ramal=countDMA();
      else var ramal=JSON.parse(sessionStorage.getItem('ramal'));
      var safety=+sessionStorage.getItem('safety');
      var pesan=+sessionStorage.getItem('pesan');
      var simpan=+sessionStorage.getItem('simpan');
      var permintaan=[];
      var totalramal=0;
      var totalkerja=0;
      var totalpermintaan=0;
      for(var x=0;x<hari.length;x++){
         permintaan[x]=Math.round(+ramal[x]/(+hari[x]));
         totalramal+=+ramal[x];
         totalkerja+=(+hari[x]);
         totalpermintaan+=permintaan[x];
      }
      // app.console.log(ctr);
      var rataramal=Math.round(totalramal/12);
      var eoq = Math.round(Math.sqrt((2*pesan*totalramal)/simpan));
      var poq=Math.round((1/rataramal)*eoq);
     
      var change=c;
      var ctr3=1;
      var iterasi=0;
      var ctr2=0;
      var sum=0;
      //app.console.log('stock='+change);
    for(var x=a;x<bulan.length;x++){
        for(var y=0;y<hari[x];y++){
          if(iterasi==0){
             y=b;
            iterasi++;
            sum +=(permintaan[x]+safety-change);
          }

          if(ctr3==poq){
                //sum+=permintaan[x];
                ctr2++;
                break;
          }
          else{
            sum+=permintaan[x];
            ctr3++;
          }
           
        }
    
        if(ctr2!=0){
          break;
        }
    }
    // app.console.log(ctr);
    return sum;
  
}

function getPorec2(a,b,c){
  let $ = require('jquery');
    const remote = require('electron').remote;
    const app = remote.app;
    
    var bulan=JSON.parse(sessionStorage.getItem('namabulan'));
    var hari=JSON.parse(sessionStorage.getItem('hari'));
    var method=+sessionStorage.getItem('method');
    if(method==5) var ramal=countLinier();
      else if(method==4)var ramal=countDES();
      else if(method==1)var ramal=countDMA();
      else var ramal=JSON.parse(sessionStorage.getItem('ramal'));
    var safety=+sessionStorage.getItem('safety');
    var pesan=+sessionStorage.getItem('pesan');
    var simpan=+sessionStorage.getItem('simpan');
    var permintaan=[];
    var totalramal=0;
    var totalkerja=0;
    var totalpermintaan=0;
    for(var x=0;x<hari.length;x++){
       permintaan[x]=Math.round(+ramal[x]/(+hari[x]));
       totalramal+=+ramal[x];
       totalkerja+=(+hari[x]);
       totalpermintaan+=permintaan[x];
    }
    
    var rataramal=Math.round(totalramal/12);
    var eoq = Math.round(Math.sqrt((2*pesan*totalramal)/simpan));
    var poq=Math.round((1/rataramal)*eoq);
    
    var change=c;
    var ctr3=1;
    var iterasi=0;
    var ctr2=0;
    var sum=0;
    
  for(var x=a;x<bulan.length;x++){
      for(var y=0;y<hari[x];y++){
        if(iterasi==0){
           y=b+1;
          iterasi++;
          sum +=(permintaan[x]+safety-change)
        }
          sum+=permintaan[x];
        
      }

  }
  // app.console.log(ctr);
  // app.console.log(sum);
  app.console.log('');
  return sum;

}

